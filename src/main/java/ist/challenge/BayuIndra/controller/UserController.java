package ist.challenge.BayuIndra.controller;

import ist.challenge.BayuIndra.dto.UsersRequestDTO;
import ist.challenge.BayuIndra.entity.Users;
import ist.challenge.BayuIndra.service.UserService;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ist")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/listUser")
    public List<Users> getAllUser() throws Exception {
        return userService.getAll();
    }
    @PostMapping("/user/create")
    public ResponseEntity<Object> createUser(@RequestBody UsersRequestDTO usersRequestDTO) {
        return this.userService.createUser(usersRequestDTO);
    }
    @PutMapping("/user/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody UsersRequestDTO usersRequestDTO){
        return userService.updateUser(id, usersRequestDTO);
    }
    @PostMapping("/Login")
    public ResponseEntity<Object> login(@RequestBody UsersRequestDTO request) {
        return userService.login(request);
    }
}
