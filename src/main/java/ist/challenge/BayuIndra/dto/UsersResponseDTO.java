package ist.challenge.BayuIndra.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersResponseDTO {
    private String username;
    private String password;

    @Override
    public String toString() {
        return "UserResponseDTO{" +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
