package ist.challenge.BayuIndra.dto;

import ist.challenge.BayuIndra.entity.Users;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersRequestDTO {

    private String username;
    private String password;

    public Users convertToEntity(){
        return Users.builder()
                .username(this.username)
                .password(this.password)
                .build();
    }
}
