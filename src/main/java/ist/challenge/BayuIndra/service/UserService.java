package ist.challenge.BayuIndra.service;

import ist.challenge.BayuIndra.dto.UsersRequestDTO;
import ist.challenge.BayuIndra.entity.Users;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<Users> getAll();
    ResponseEntity<Object> createUser(UsersRequestDTO usersRequestDTO);
    Optional<Users> getUserById(Long id);
    ResponseEntity<Object> updateUser(Long id,UsersRequestDTO requestDTO);
    ResponseEntity<Object> login(UsersRequestDTO request);
}
