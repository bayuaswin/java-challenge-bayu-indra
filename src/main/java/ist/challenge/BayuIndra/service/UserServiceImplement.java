package ist.challenge.BayuIndra.service;

import ist.challenge.BayuIndra.dto.UsersRequestDTO;
import ist.challenge.BayuIndra.dto.UsersResponseDTO;
import ist.challenge.BayuIndra.entity.Users;
import ist.challenge.BayuIndra.exception.ResourceNotFoundException;
import ist.challenge.BayuIndra.handler.ResponseHandler;
import ist.challenge.BayuIndra.repo.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImplement implements UserService {
    UserRepo userRepo;

    @Override
    public List<Users> getAll(){
        List<Users> user = userRepo.findAll();
        if(user.isEmpty()){
            throw new RuntimeException("Belum ada data");
        }
        return userRepo.findAll();
    }

    @Override
    public ResponseEntity<Object> createUser(UsersRequestDTO usersRequestDTO) {
        try {
            if (userRepo.existsByUsername(usersRequestDTO.getUsername())) {
                throw new Exception("Username sudah terpakai");
            }
            if (userRepo.existsByPassword(usersRequestDTO.getPassword())) {
                throw new Exception("Password sudah terpakai");
            }
            Users users = usersRequestDTO.convertToEntity();
            userRepo.save(users);
            UsersResponseDTO userResult = users.convertToResponse();
            return ResponseHandler.generateResponse("Successfully Created User!", HttpStatus.CREATED, userResult);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.CONFLICT, null);
        }
    }

    @Override
    public Optional<Users> getUserById(Long id) {
        Optional<Users> optionalUser = userRepo.findById(id);
        if(optionalUser.isEmpty()){
            throw new ResourceNotFoundException("User not exist with id :" + id);
        }
        return this.userRepo.findById(id);
    }

    @Override
    public ResponseEntity<Object> updateUser(Long id, UsersRequestDTO requestDTO) {
        try {
            Users users = userRepo.getById(id);
            users.setUsername(requestDTO.getUsername());
            users.setPassword(requestDTO.getPassword());
            Users updateUsers = userRepo.save(users);
            UsersResponseDTO result = updateUsers.convertToResponse();
            return ResponseHandler.generateResponse("Successfully Updated User",HttpStatus.CREATED, result);
        }catch(Exception e){
            return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.CONFLICT,"Password tidak boleh sama dengan password sebelumnya");
        }
    }

    @Override
    public ResponseEntity<Object> login(UsersRequestDTO request) {
        try {
            if(request.getUsername().isEmpty() || request.getPassword().isEmpty()){
                throw new ResourceNotFoundException("Username atau password kosong");
            }
            Users users = userRepo.findByUsername(request.getUsername()).orElseThrow();
            if(!request.getPassword().equals(users.getPassword())){
                throw new ResourceNotFoundException("Password Salah");
            }
            return ResponseHandler.generateResponse("Sukses Login", HttpStatus.OK, users);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, null);
        }
    }

}
