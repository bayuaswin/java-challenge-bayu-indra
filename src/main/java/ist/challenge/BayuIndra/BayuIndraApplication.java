package ist.challenge.BayuIndra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BayuIndraApplication {

	public static void main(String[] args) {
		SpringApplication.run(BayuIndraApplication.class, args);
	}

}
