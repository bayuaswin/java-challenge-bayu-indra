package ist.challenge.BayuIndra.entity;

import ist.challenge.BayuIndra.dto.UsersResponseDTO;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
@Entity
@Builder
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long userId;
    @Column(name = "username",nullable = false,unique = true,length = 25)
    private String username;
    @Column(name = "password",nullable = false,length = 25)
    private String password;

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public UsersResponseDTO convertToResponse() {
        return UsersResponseDTO.builder()
                .username(this.username)
                .password(this.password)
                .build();
    }
}
